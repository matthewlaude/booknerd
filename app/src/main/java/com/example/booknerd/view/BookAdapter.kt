package com.example.booknerd.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.booknerd.databinding.ItemBookBinding
import com.example.booknerd.model.BookRepo
import com.example.booknerd.model.local.entity.Book

class BookAdapter(private val books: List<Book>, val repo: BookRepo
) : RecyclerView.Adapter<BookAdapter.BookViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = BookViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        val url = books[position]
        holder.loadBookImage(url, repo)
    }

    override fun getItemCount() = books.size

    class BookViewHolder(
        private val binding: ItemBookBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadBookImage(book: Book, repo: BookRepo) {
//            binding.cvBook.load(book.title)
            binding.tvBookItem.text = book.title

//            CoroutineScope(Dispatchers.IO).launch {
//                repo.bookDao.update(book)
//            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemBookBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> BookViewHolder(binding) }
        }
    }
}