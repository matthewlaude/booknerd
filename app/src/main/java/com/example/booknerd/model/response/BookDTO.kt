package com.example.booknerd.model.response

import com.google.gson.annotations.SerializedName

class BookDTO(
    val booksItems: List<BookItem>
) {
    data class BookItem(
        val title: String
    )
}
