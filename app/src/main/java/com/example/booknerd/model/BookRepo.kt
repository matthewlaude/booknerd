package com.example.booknerd.model

import android.content.Context
import com.example.booknerd.model.local.BookDatabase
import com.example.booknerd.model.local.entity.Book
import com.example.booknerd.model.remote.BookService
import com.example.booknerd.model.response.BookDTO
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BookRepo @Inject constructor(
    val bookService: BookService, @ApplicationContext context: Context
) {
    val bookDao = BookDatabase.getInstance(context).bookDao()

    suspend fun getBooks(): List<Book> = withContext(Dispatchers.IO) {
        val cachedBooks: List<Book> = bookDao.getAll()

        return@withContext cachedBooks.ifEmpty {
            val books: List<Book> = bookService
                .getBooks()
                .map { Book(title = it.title) }
            bookDao.insert(books)
            return@ifEmpty books
        }
    }
}