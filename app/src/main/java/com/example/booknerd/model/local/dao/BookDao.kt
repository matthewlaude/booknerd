package com.example.booknerd.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.booknerd.model.local.entity.Book

@Dao
interface BookDao {

    @Query("SELECT * FROM book")
    suspend fun getAll(): List<Book>

    @Insert
    suspend fun insert(book: List<Book>)

//    @Update
//    suspend fun update(book: Book)
}