package com.example.booknerd.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.booknerd.model.local.dao.BookDao
import com.example.booknerd.model.local.entity.Book

@Database(entities = [Book::class], version = 2)
abstract class BookDatabase : RoomDatabase()  {

    abstract fun bookDao(): BookDao

    companion object {

        private const val DATABASE_NAME = "book.db"

        @Volatile
        private var instance: BookDatabase? = null

        fun getInstance(context: Context): BookDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }
        private fun buildDatabase(context: Context): BookDatabase {
            return Room.databaseBuilder(context, BookDatabase::class.java, DATABASE_NAME).build()
        }
    }
}