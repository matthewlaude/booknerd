package com.example.booknerd.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.booknerd.model.BookRepo
import com.example.booknerd.model.local.entity.Book
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class BookViewModel @Inject constructor(
    private val repo: BookRepo
) : ViewModel() {

    val state: LiveData<BookState> = liveData {
        emit(BookState())
        val books = repo.getBooks()
        emit(BookState(books = books))
    }

    data class BookState(
        val books: List<Book> = emptyList()
    )
}